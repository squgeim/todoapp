export default {
    TODO: '/todos',
    TODO_BY_ID: id => `/todos/${id}`,
};