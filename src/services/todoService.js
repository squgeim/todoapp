import API from '../constants/api';
import * as http from '../utils/http';

export const createTodo = todo => http.post(API.TODO, todo);