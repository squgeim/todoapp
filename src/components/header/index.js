import React from 'react';
import jss from 'jss';

const { classes } = jss.createStyleSheet({
  header: {
    height: '50px',
    background: 'black',
    color: 'white',
    display: 'flex',
    'justify-content': 'center',
    'align-items': 'center',
  },
}).attach();

const Header = () => (
  <div className={classes.header}>
    Todo App
  </div>
);

export default Header;
