import jss from 'jss';
import React from 'react';

const { classes } = jss.createStyleSheet({
  isCompleted: {
    'text-decoration': 'line-through'
  },
}).attach();

const TodoList = ({ todos }) => (
  <div>
    <ul>
      {todos.map((todo, i) => (
        <li key={i} className={todo.isCompleted ? classes.isCompleted : ''}>
          <strong>{todo.title}</strong>
          <p>{todo.description}</p>
        </li>
      ))}
    </ul>
  </div>
);

export default TodoList;
