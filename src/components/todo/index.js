import React from 'react';

import Header from '../header';
import TodoList from './todoList';
import InputTodo from './inputTodo';

import * as todoUtil from '../../utils/todoUtil';
import * as todoService from '../../services/todoService';

class Todo extends React.Component {
  constructor() {
    super();

    this.state = {
      todos: []
    };

    this._addTodoItem = this._addTodoItem.bind(this);
  }

  _addTodoItem(description) {
    const todo = todoUtil.buildTodoItem(description);

    todoService
      .createTodo(todo)
      .then(response => {
        this.setState(prevState => ({
          todos: [response, ...prevState.todos],
        }));
      })
      .catch(err => {

      })
  }

  render() {
    return (
      <div>
        <Header />
        <InputTodo createTodo={this._addTodoItem} />
        <TodoList todos={this.state.todos} />
      </div>
    );
  }
};

export default Todo;
