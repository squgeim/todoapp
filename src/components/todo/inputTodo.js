import React from 'react';

class InputTodo extends React.Component {
  constructor() {
    super();

    this.state = {
      value: ''
    };

    this._createTodo = this._createTodo.bind(this);
  }

  _createTodo(value) {
    this.setState({
      value: '',
    });

    typeof this.props.createTodo === 'function' &&
      this.props.createTodo(value);
  }

  render() {
    return (
      <div>
        <input
          type="text"
          value={this.state.value}
          onChange={e => {
            this.setState({
              value: e.target.value,
            });
          }}
          onKeyDown={e => {
            if (e.keyCode === 13) {
              this._createTodo(e.target.value)
            }
          }}
        />
      </div>
    )
  }
}

export default InputTodo;
