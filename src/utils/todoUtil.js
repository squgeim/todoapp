
export const buildTodoItem = (value, isCompleted = false) => {
    const [title, ...descriptionArr] = value.split('\n');

    const description = descriptionArr
        ? descriptionArr.join('\n')
        : '';

    return {
        title,
        description,
        isCompleted,
    }
}