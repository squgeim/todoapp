import axiosPackage from 'axios';

const axios = axiosPackage.create({
    baseURL: 'https://todo-simple-api.herokuapp.com/',
    responseType: 'json',
});

axios.interceptors.response.use(res => res.data);

export const get = (path, params) => {
    return axios({
        method: 'get',
        url: path,
        params,
    }).then(res => res.data);
}

export const post = (path, body) => {
    return axios({
        method: 'post',
        url: path,
        data: body,
    }).then(res => res.data);
}